import h5py 
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from sklearn.preprocessing import MinMaxScaler


#f hat 100k keys, jeder key hat eine hdf5 group als value
#jede group hat 2 keys. in key 'X' ist ein 2d numpy array
#welches die 200x200 pixel des bildes speichert (dset[70:71,:] 
# zugriff auf zeile 71) und in key'Y' sind die drei 
# korrespondierenden achsenwerte der kamera X, Y und Z

path = "training_data_alignment.h5"

def prepare_data(path):
    # load h5 file
    sims = 50000  # only half the data
    x = []
    y = []
    with h5py.File(path, 'r') as f:
        for key in range(sims):
            X = np.array(f['/' + str(key) + '/X'])
            Y = np.array(f['/' + str(key) + '/Y'])
            X = MinMaxScaler().fit_transform(X) #normalise the data between 0 and 1
            #Y = MinMaxScaler().fit_transform(Y)
            #print(Y[:])
            if np.amax(X) > 0:  # ignore the blank images (no data)
                x.append(X)
                y.append(Y)
    
    # TODO split test and train data

    #device = torch.device("cpu")
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    print(device)
    x = torch.tensor(x, dtype=torch.float32, device=device)  # input
    print('x.shape', np.shape(x))
    print('y.shape', np.shape(y))
    print(x.element_size() * x.nelement())

#TODO anpassen an unsere Situation
class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)
    
    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = torch.flatten(x, 1) # flatten all dimensions except batch
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

net = Net()
print(net)

prepare_data(path)       

